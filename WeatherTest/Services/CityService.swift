//
//  CityService.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import Foundation

class CityService {
    static let instance = CityService()
    
    private let categories = [
        Category(title:"LONDON"),
        Category(title:"PARIS"),
        Category(title:"VIENNA"),
        Category(title:"BUDAPEST"),
        Category(title:"AMSTERDAM"),
        Category(title:"BERLIN"),
        Category(title:"STOCKHOLM"),
        Category(title:"ROME"),
        Category(title:"LISBON"),
        Category(title:"MADRID"),
        Category(title:"MURMANSK")
    ]
    
    func getCategories() -> [Category]{
        return categories
    }
}
