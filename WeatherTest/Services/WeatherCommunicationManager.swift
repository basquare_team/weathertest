//
//  WeatherCommunicationManager.swift
//  WeatherTest
//
//  Created by Taras Sheremet on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import Foundation

enum WeatherCommunicationManagerError: Error {
    case communicationError
}

class WeatherCommunicationManager {
    static private var _defaultManager:WeatherCommunicationManager? = nil;
    
    static func defaultManager() -> WeatherCommunicationManager {
        if _defaultManager == nil {
            _defaultManager = WeatherCommunicationManager();
        }
        return _defaultManager!;
    }
    
    private func apiCall(_ method: String, params : [String:String], compleation: (Any?, Error?) -> Void) {
        var url = "\(weatherServerUrl)\(weatherMethod)?appid=\(weatherApiKey)&units=metric";
        
        let limiter = "&";
        for (param, value) in params {
            url = url + "\(limiter)\(param)=\(String(describing: value.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlFragmentAllowed)!))"
        }
        
        do {
            let data = try Data(contentsOf: URL.init(string: url)!)
            let wetherData = try JSONSerialization.jsonObject(with: data, options: [])
            compleation(try Weather.init(dictionaryData:wetherData), nil)
        } catch {
            compleation(nil, WeatherCommunicationManagerError.communicationError)
        }
        
    }
    
    func weatherForCity(_ city: String, compleation: (_ weather: Any?, Error?) -> Void) {
        self.apiCall(weatherMethod, params: [weatherMethodCityParam:city], compleation: compleation)
    }
}
