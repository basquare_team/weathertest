//
//  Weather.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import Foundation

enum WeatherError : Error {
    case wrongData
}

struct Weather {
    
    private(set) public var city: String!
    private(set) public var temperature: Int!
    private(set) public var wind: Float!
    private(set) public var humidity: Int!
    private(set) public var weather: String!
    
    init(dictionaryData : Any ) throws {
        let values = dictionaryData as! [String : Any]
        
        let main = values["main"] as! [String : Any]
        self.temperature = (main["temp"] as! NSNumber).intValue
        self.humidity = main["humidity"] as! Int!
        
        let wind = values["wind"] as! [String : Any]
        self.wind = wind["speed"] as! Float!
        self.city = values["name"] as! String!
        
        let weatherArray = values["weather"] as! [Any]
        let curentWeather = weatherArray[0] as! [String:Any]
        self.weather = curentWeather["main"] as! String
    }
    

    init(city : String, temperature: Int, wind: Float, humidity: Int, weather:String ) {
        self.temperature = temperature
        self.humidity = humidity
        self.wind = wind
        self.city = city
        self.weather = weather
    }
}
