//
//  Categoty.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import Foundation

struct Category {
    private(set) public var title: String!
    
    init(title: String) {
        self.title = title
    }
}
