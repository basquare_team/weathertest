//
//  CityCell.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var CityLabel: UILabel!
    
    func updateViews (category: Category) {
        CityLabel.text = category.title
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
