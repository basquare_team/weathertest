//
//  Constants.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import UIKit

typealias ComplisionHandler = (_ Success: Bool) -> ()

let weatherApiKey : String = "d2bd923726d8850b7677856f80cb52cd"
let weatherServerUrl : String = "http://api.openweathermap.org/data/2.5/"
let weatherMethod : String = "weather"
let weatherMethodCityParam : String = "q"

//URL CONSTANTS
let BASE_URL = ""
let URL_CITIES = "\(weatherServerUrl)"

// USER DEFAULTS
let TOKEN_KEY = "token"

let aboveZerroColor : UIColor = UIColor.red
let belowZerroColor : UIColor = UIColor.blue

