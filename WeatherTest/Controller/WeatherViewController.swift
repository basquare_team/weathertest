//
//  WeatherViewController.swift
//  WeatherTest
//
//  Created by YaroslavKoberskiy on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    var weather : Weather?;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.weather != nil {
            self.setWeatherData(self.weather!)
        }
    }

    @IBAction func onClose(_ sender: Any) {
        self .dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setWeatherData(_ weather: Weather) -> Void {
        self.weather = weather
        
        _ = self.view
        
        self.cityLabel.text = weather.city
        self.temperatureLabel.text = "\(weather.temperature!)"
        self.temperatureLabel.backgroundColor = self.color(forTemerature: weather.temperature!)
        
        if weather.weather == "Clouds" {
            self.weatherImage.image = UIImage(named:"cloudy")
        } else if weather.weather == "Rain" {
            self.weatherImage.image = UIImage(named:"rainy")
        } else {
            self.weatherImage.image = UIImage(named:"sunny")
        }
        
    }
    
    func color(forTemerature temperature : Int) -> UIColor {
        if temperature > 0 {
            return aboveZerroColor
        }
        else {
            return belowZerroColor
        }
    }
}
