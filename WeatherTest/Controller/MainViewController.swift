//
//  ViewController.swift
//  WeatherTest
//
//  Created by Taras Sheremet on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import UIKit

enum CityError: Error {
    case lostCity
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var citiesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        citiesTableView.delegate = self
        citiesTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CityService.instance.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell") as? CityCell {
        let category = CityService.instance.getCategories()[indexPath.row]
            cell.updateViews(category: category)
            return cell
        } else {
            return CityCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let city = CityService.instance.getCategories()[indexPath.row].title
        let _:Error

        WeatherCommunicationManager.defaultManager().weatherForCity(city!) { (weather, error) in
            if error == nil {
                DispatchQueue.main.async {
                    let viewController:WeatherViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cityWeatherVC") as! WeatherViewController
                    
                    viewController.setWeatherData(weather as! Weather!);
                    viewController.modalPresentationStyle = .overCurrentContext;
                    self.present(viewController, animated: true, completion: {
                        
                    })
                }
            }
            else {
                let alert = UIAlertController(title: "Error", message: "Cannot get weather!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

