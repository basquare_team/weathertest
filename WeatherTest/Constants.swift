//
//  Constants.swift
//  WeatherTest
//
//  Created by Taras Sheremet on 1/29/18.
//  Copyright © 2018 Taras Sheremet. All rights reserved.
//

import Foundation

let weatherApiKey : String = "d2bd923726d8850b7677856f80cb52cd"
let weatherServerUrl : String = "http://samples.openweathermap.org/data/2.5/"
let weatherMethod : String = "weather"
let weatherMethodCityParam : String = "q"
